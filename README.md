# Application behind search.joinpeertube.org

## Dev

```terminal
$ yarn install --pure-lockfile
```

Indexes in Meilisearch are automatically created by PeerTube at startup.

Run simultaneously (for example with 3 terminals):

```terminal
$ npm run tsc -- -w
```

```terminal
$ node dist/server
```

```
$ cd client && npm run dev
```

Then open http://localhost:8080.

### Add locale

Add the locale in `client/src/main.ts` and `client/gettext.config.js` and run `npm run i18n:update`.

## Production

### Installation

Install dependencies:
  * NodeJS (v18)
  * MeiliSearch

```terminal
$ git clone https://framagit.org/framasoft/peertube/search-index.git /var/www/peertube-search-index
$ cd /var/www/peertube-search-index
$ yarn install --pure-lockfile
$ npm run build
$ cp config/default.yaml config/production.yaml
$ vim config/production.yaml
$ NODE_ENV=production NODE_CONFIG_DIR=/var/www/peertube-search-index/config node dist/server.js
```

### Upgrade

```
cd /var/www/peertube-search-index && \
  git pull origin master && \
  yarn install --pure-lockfile && \
  npm run build && \
  sudo systemctl restart peertube-search-index.service
```
