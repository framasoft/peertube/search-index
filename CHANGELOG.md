# v1.0.0

 * Use [Meilisearch](https://www.meilisearch.com) instead of Elastic Search
   * See https://framagit.org/framasoft/peertube/search-index/-/issues/50 for more information
   * Migration from Elastic Search is not implemented, just run another PeerTube search index to index videos/channels/playlists and when it's done you can switch from the old index and the new one
   * Better global search relevancy
   * Fix search with accentuated characters
   * Improve search relevancy with non latin search
   * Support verbatim search

