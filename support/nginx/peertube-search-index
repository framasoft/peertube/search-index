server {
  listen 80;
  listen [::]:80;
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name domain.tld;

  access_log /var/log/nginx/peertube_search_index_access.log;
  error_log /var/log/nginx/peertube_search_index_error.log;

  ssl_certificate      /etc/letsencrypt/live/domain.tld/fullchain.pem;
  ssl_certificate_key  /etc/letsencrypt/live/domain.tld/privkey.pem;

  if ($scheme = http) {
     return 301 https://$server_name$request_uri;
  }

  root /var/www/peertube-search-index;

  location = / {
    try_files /index.template.html =404;
  }

  location ^~ '/.well-known/acme-challenge' {
    default_type "text/plain";
    root /var/www/certbot;
  }

  location / {
    proxy_pass http://localhost:3234;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }
}
