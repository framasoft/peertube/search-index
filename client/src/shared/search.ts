import axios from 'axios'
import { ResultList, VideoChannelsSearchQuery, VideoPlaylistsSearchQuery, VideosSearchQuery } from '@peertube/peertube-types'
import { APIVideoChannel } from '../../../server/types/channel.model'
import { APIPlaylist } from '../../../server/types/playlist.model'
import { APIVideo } from '../../../server/types/video.model'
import { buildApiUrl } from './utils'

const baseVideosPath = '/api/v1/search/videos'
const baseVideoChannelsPath = '/api/v1/search/video-channels'
const baseVideoPlaylistsPath = '/api/v1/search/video-playlists'

function searchVideos (options: VideosSearchQuery) {
  const axiosOptions = {
    params: {
      ...options,

      search: options.search || undefined
    }
  }

  return axios.get<ResultList<APIVideo>>(buildApiUrl(baseVideosPath), axiosOptions)
    .then(res => res.data)
}

function searchVideoChannels (options: VideoChannelsSearchQuery) {
  const axiosOptions = {
    params: {
      ...options,

      search: options.search || undefined
    }
  }

  return axios.get<ResultList<APIVideoChannel>>(buildApiUrl(baseVideoChannelsPath), axiosOptions)
    .then(res => res.data)
}

function searchVideoPlaylists (options: VideoPlaylistsSearchQuery) {
  const axiosOptions = {
    params: {
      ...options,

      search: options.search || undefined
    }
  }

  return axios.get<ResultList<APIPlaylist>>(buildApiUrl(baseVideoPlaylistsPath), axiosOptions)
    .then(res => res.data)
}

export {
  searchVideos,
  searchVideoChannels,
  searchVideoPlaylists
}
