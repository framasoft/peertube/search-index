function parseHTML (html: string) {
  const div = document.createElement('div')
  div.innerHTML = html

  return div as HTMLDivElement
}

function getChildNodes (node: HTMLElement) {
  return node.childNodes as NodeListOf<HTMLElement>
}

function getAttribute (node: HTMLElement, key: string) {
  return node.getAttribute(key)
}

function getClass (node: HTMLElement) {
  return node.className
}

function getText (node: HTMLElement) {
  return node.textContent
}

function nodeIs (node: HTMLElement, tagName: string) {
  if (tagName === 'TEXT') {
    return node.nodeType === 3
  }

  return node.tagName === tagName
}

export {
  parseHTML,

  getChildNodes,
  getClass,
  getAttribute,
  getText,
  nodeIs
}
