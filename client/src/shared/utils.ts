function buildApiUrl (path: string) {
  const normalizedPath = path.startsWith('/') ? path : '/' + path

  const base = import.meta.env.VITE_APP_API_URL || ''
  return base + normalizedPath
}

function durationToString (duration: number) {
  const hours = Math.floor(duration / 3600)
  const minutes = Math.floor((duration % 3600) / 60)
  const seconds = duration % 60

  const minutesPadding = minutes >= 10 ? '' : '0'
  const secondsPadding = seconds >= 10 ? '' : '0'
  const displayedHours = hours > 0 ? hours.toString() + ':' : ''

  return (
    displayedHours + minutesPadding + minutes.toString() + ':' + secondsPadding + seconds.toString()
  ).replace(/^0/, '')
}

function pageToAPIParams (page: number, itemsPerPage: number) {
  const start = (page - 1) * itemsPerPage
  const count = itemsPerPage

  return { start, count }
}

function durationRangeToAPIParams (durationRange: string) {
  if (!durationRange) {
    return { durationMin: undefined, durationMax: undefined }
  }

  const fourMinutes = 60 * 4
  const tenMinutes = 60 * 10

  switch (durationRange) {
    case 'short':
      return { durationMin: undefined, durationMax: fourMinutes}

    case 'medium':
      return { durationMin: fourMinutes, durationMax: tenMinutes }

    case 'long':
      return { durationMin: tenMinutes, durationMax: undefined }

    default:
      console.error('Unknown duration range %s', durationRange)
      return { durationMin: undefined, durationMax: undefined }
  }
}

function publishedDateRangeToAPIParams (publishedDateRange: string) {
  const empty = { startDate: undefined, endDate: undefined }

  if (!publishedDateRange) return empty

  // today
  const date = new Date()
  date.setHours(0, 0, 0, 0)

  switch (publishedDateRange) {
    case 'today':
      break

    case 'last_7days':
      date.setDate(date.getDate() - 7)
      break

    case 'last_30days':
      date.setDate(date.getDate() - 30)
      break

    case 'last_365days':
      date.setDate(date.getDate() - 365)
      break

    case 'any_published_date':
      return empty

    default:
      console.error('Unknown published date range %s', publishedDateRange)
      return empty
  }

  return { startDate: date.toISOString(), endDate: undefined }
}

function extractTagsFromQuery <T> (value: T | T[]) {
  if (!value) return []

  if (Array.isArray(value)) {
    return value.map(v => ({ text: v }))
  }

  return [ { text: value } ]
}

function extractQueryToStringArray (value: string | string[]) {
  if (!value) return undefined

  if (Array.isArray(value)) return value

  return [ value ]
}

function extractQueryToIntArray (value: string | string[]) {
  if (!value) return undefined

  if (Array.isArray(value)) return value.map(v => parseInt(v))

  return [ parseInt(value) ]
}

function extractQueryToInt (value: string) {
  if (!value) return undefined

  return parseInt(value)
}

function extractQueryToBoolean (value: string) {
  if (value === 'true') return true
  if (value === 'false') return false

  return undefined
}

export {
  buildApiUrl,
  durationToString,
  publishedDateRangeToAPIParams,
  pageToAPIParams,
  durationRangeToAPIParams,
  extractTagsFromQuery,
  extractQueryToStringArray,
  extractQueryToIntArray,
  extractQueryToInt,
  extractQueryToBoolean
}

