export default {
  methods: {
    async lazyRenderMarkdown (markdown: string) {
      const { renderMarkdown } = await import('../shared/markdown-render')

      return renderMarkdown(markdown)
    }
  }
}
