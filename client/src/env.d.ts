/// <reference types="vite/client" />

import { Language } from 'vue3-gettext'

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties extends Pick<Language, "$gettext" | "$pgettext" | "$ngettext" | "$npgettext"> {
    $language: Language;

    lazyRenderMarkdown: (markdown: string) => Promise<string>
  }
}
