msgid ""
msgstr ""
"PO-Revision-Date: 2023-08-03 13:06+0000\n"
"Last-Translator: Milo Ivir <mail@milotype.de>\n"
"Language-Team: Croatian <https://weblate.framasoft.org/projects/"
"peertube-search-index/client/hr/>\n"
"Language: hr\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.18.2\n"

#: src/views/Search.vue:64 src/views/Search.vue:65 src/views/Search.vue:68
#: src/views/Search.vue:72
msgid "%{totalChannels} channel"
msgid_plural "%{totalChannels} channels"
msgstr[0] "%{totalChannels} kanal"
msgstr[1] "%{totalChannels} kanala"
msgstr[2] "%{totalChannels} kanala"

#: src/views/Search.vue:84 src/views/Search.vue:85 src/views/Search.vue:88
#: src/views/Search.vue:96
msgid "%{totalPlaylists} playlist"
msgid_plural "%{totalPlaylists} playlists"
msgstr[0] "%{totalPlaylists} playlista"
msgstr[1] "%{totalPlaylists} playliste"
msgstr[2] "%{totalPlaylists} playlista"

#: src/views/Search.vue:38 src/views/Search.vue:39 src/views/Search.vue:42
msgid "%{totalResults} result found:"
msgid_plural "%{totalResults} results found:"
msgstr[0] "%{totalResults} rezultat pronađen:"
msgstr[1] "%{totalResults} rezultata pronađena:"
msgstr[2] "%{totalResults} rezultata pronađeno:"

#: src/views/Search.vue:44 src/views/Search.vue:45 src/views/Search.vue:48
msgid "%{totalVideos} video"
msgid_plural "%{totalVideos} videos"
msgstr[0] "%{totalVideos} video"
msgstr[1] "%{totalVideos} videa"
msgstr[2] "%{totalVideos} videa"

#: src/components/ChannelResult.vue:25 src/components/ChannelResult.vue:26
#: src/components/ChannelResult.vue:29
msgid "%{videosCount} video"
msgid_plural "%{videosCount} videos"
msgstr[0] "%{videosCount} video"
msgstr[1] "%{videosCount} videa"
msgstr[2] "%{videosCount} videa"

#: src/components/PlaylistResult.vue:118
msgid "%{videosLength} video"
msgid_plural "%{videosLength} videos"
msgstr[0] "%{videosLength} video"
msgstr[1] "%{videosLength} videa"
msgstr[2] "%{videosLength} videa"

#: src/components/SearchWarning.vue:7 src/components/SearchWarning.vue:8
msgid ""
"<strong>%{indexName}</strong> displays videos and channels that match your "
"search but is not the publisher, nor the owner."
msgstr ""
"<strong>%{indexName}</strong> prikazuje videa i kanale koji odgovaraju "
"tvojoj pretrazi, ali nije izdavač niti vlasnik."

#: src/components/Footer.vue:6 src/components/Footer.vue:7
msgid "A free software to take back control of your videos"
msgstr "Slobodan softver za preuzimanje kontrole nad tvojim videima"

#: src/components/Header.vue:17 src/components/Header.vue:18
msgid ""
"A search engine of <a class=\"peertube-link\" href=\"https://joinpeertube."
"org\" target=\"_blank\">PeerTube</a> videos, channels and playlists"
msgstr ""
"Tražilica <a class=\"peertube-link\" href=\"https://joinpeertube.org\" "
"target=\"_blank\">PeerTube</a> videa, kanala i playlista"

#: src/components/Filters.vue:296
msgid "Activism"
msgstr "Aktivizam"

#: src/components/Filters.vue:232
msgid "Add tag"
msgstr "Dodaj oznaku"

#: src/components/Filters.vue:151
msgid "All of these tags"
msgstr "Sve ove oznake"

#: src/components/PlaylistResult.vue:42 src/components/PlaylistResult.vue:43
#: src/components/PlaylistResult.vue:46
msgid "and updated on"
msgstr "i aktualizirano"

#: src/components/Filters.vue:298
msgid "Animals"
msgstr "Životinje"

#: src/components/Filters.vue:239 src/components/Filters.vue:264
msgid "Any"
msgstr "Bilo koji"

#: src/components/Filters.vue:224
msgid "Apply filters"
msgstr "Primjeni filtre"

#: src/components/Filters.vue:286
msgid "Art"
msgstr "Umjetnost"

#: src/components/Filters.vue:306
msgid "Attribution"
msgstr "Imenovanje"

#: src/components/Filters.vue:308
msgid "Attribution - No Derivatives"
msgstr "Imenovanje – Bez prerada"

#: src/components/Filters.vue:309
msgid "Attribution - Non Commercial"
msgstr "Imenovanje – Nekomercijalno"

#: src/components/Filters.vue:311
msgid "Attribution - Non Commercial - No Derivatives"
msgstr "Imenovanje – Nekomercijalno – Bez prerada"

#: src/components/Filters.vue:310
msgid "Attribution - Non Commercial - Share Alike"
msgstr "Imenovanje, Nekomercijalno – Dijeli pod istim uvjetima"

#: src/components/Filters.vue:307
msgid "Attribution - Share Alike"
msgstr "Imenovanje – Dijeli pod istim uvjetima"

#: src/components/SortButton.vue:6 src/components/SortButton.vue:7
msgid "Best match"
msgstr "Najbolje poklapanje"

#: src/components/Filters.vue:322
msgid "Català"
msgstr "Català"

#: src/components/Filters.vue:94
msgid "Category"
msgstr "Kategorija"

#: src/components/Filters.vue:323
msgid "Čeština"
msgstr "Čeština"

#: src/components/InterfaceLanguageDropdown.vue:51
msgid "Change interface language"
msgstr "Promijeni jezik korisničkog sučelja"

#: src/components/ChannelResult.vue:29 src/components/ChannelResult.vue:30
#: src/components/ChannelResult.vue:33
msgid "Channel created on platform"
msgstr "Kanal stvoren na platformi"

#: src/components/Header.vue:7
msgid "Come back to homepage"
msgstr "Vrati se na početnu stranicu"

#: src/components/Filters.vue:291
msgid "Comedy"
msgstr "Komedija"

#: src/components/PlaylistResult.vue:39 src/components/PlaylistResult.vue:40
#: src/components/PlaylistResult.vue:43
msgid "Created by"
msgstr "Autor"

#: src/components/Filters.vue:326
msgid "Deutsch"
msgstr "Deutsch"

#: src/components/Header.vue:25 src/components/Header.vue:26
msgid ""
"Developed by <a class=\"peertube-link\" href=\"https://framasoft.org\" "
"target=\"_blank\">Framasoft</a>"
msgstr ""
"Razvijeno od <a class=\"peertube-link\" href=\"https://framasoft.org\" "
"target=\"_blank\">Framasofta</a>"

#: src/components/ChannelResult.vue:83
msgid "Discover this channel on %{host}"
msgstr "Otkrij ovaj kanal na %{host}"

#: src/components/Filters.vue:101 src/components/Filters.vue:102
msgid "Display all categories"
msgstr "Prikaži sve kategorije"

#: src/components/Filters.vue:139 src/components/Filters.vue:140
msgid "Display all languages"
msgstr "Prikaži sve jezike"

#: src/components/Filters.vue:120 src/components/Filters.vue:121
msgid "Display all licenses"
msgstr "Prikaži sve licence"

#: src/views/Search.vue:75 src/views/Search.vue:76 src/views/Search.vue:79
#: src/views/Search.vue:83
msgid "Display more channels"
msgstr "Prikaži još kanala"

#: src/views/Search.vue:107 src/views/Search.vue:95 src/views/Search.vue:96
#: src/views/Search.vue:99
msgid "Display more playlists"
msgstr "Prikaži još playlista"

#: src/views/Search.vue:55 src/views/Search.vue:56 src/views/Search.vue:59
msgid "Display more videos"
msgstr "Prikaži još videa"

#: src/components/Filters.vue:47 src/components/Filters.vue:48
msgid "Display only"
msgstr "Prikaži samo"

#: src/components/Filters.vue:4 src/components/Filters.vue:5
msgid "Display sensitive content"
msgstr "Prikaži neprikladne sadržaje"

#: src/components/Filters.vue:80 src/components/Filters.vue:81
msgid "Duration"
msgstr "Trajanje"

#: src/components/Filters.vue:295
msgid "Education"
msgstr "Obrazovanje"

#: src/components/Filters.vue:318
msgid "English"
msgstr "English"

#: src/components/Filters.vue:292
msgid "Entertainment"
msgstr "Zabava"

#: src/components/Filters.vue:329
msgid "Español"
msgstr "Español"

#: src/components/Filters.vue:324
msgid "Esperanto"
msgstr "Esperanto"

#: src/components/Filters.vue:321
msgid "Euskara"
msgstr "Euskara"

#: src/components/SearchInput.vue:14 src/components/SearchInput.vue:15
#: src/components/SearchInput.vue:26 src/components/SearchInput.vue:36
msgid "Explore!"
msgstr "Traži!"

#: src/components/Filters.vue:284
msgid "Films"
msgstr "Filmovi"

#: src/components/Filters.vue:24 src/components/Filters.vue:25
msgid "Filter by result type"
msgstr "Filtriraj po vrsti rezultata"

#: src/views/Search.vue:22 src/views/Search.vue:23
msgid "Filters"
msgstr "Filtri"

#: src/components/Filters.vue:300
msgid "Food"
msgstr "Hrana"

#: src/components/Filters.vue:319
msgid "Français"
msgstr "Français"

#: src/components/Filters.vue:331
msgid "Gàidhlig"
msgstr "Gàidhlig"

#: src/components/Filters.vue:289
msgid "Gaming"
msgstr "Igranje"

#: src/components/ActorMiniature.vue:45
msgid "Go on this account page"
msgstr "Idi na ovu stranicu računa"

#: src/components/ActorMiniature.vue:43
msgid "Go on this channel page"
msgstr "Idi na ovu stranicu kanala"

#: src/components/Filters.vue:294
msgid "How To"
msgstr "Upute"

#: src/components/SearchWarning.vue:15 src/components/SearchWarning.vue:16
msgid ""
"If you notice any problems with a video, report it to the administrators on "
"the PeerTube website where the video is published."
msgstr ""
"Ako primijetiš probleme s videom, prijavi to administratorima na PeerTube "
"web-stranici na kojoj je video objavljen."

#: src/components/PlaylistResult.vue:48 src/components/PlaylistResult.vue:49
#: src/components/PlaylistResult.vue:52 src/components/VideoResult.vue:37
#: src/components/VideoResult.vue:38 src/components/VideoResult.vue:41
msgid "In channel"
msgstr "U kanalu"

#: src/components/Filters.vue:327
msgid "Italiano"
msgstr "Italiano"

#: src/components/Filters.vue:228 src/components/SearchInput.vue:59
msgid "Keyword, channel, video, playlist, etc."
msgstr "Lozinka, kanal, video, playlista itd."

#: src/components/Filters.vue:299
msgid "Kids"
msgstr "Djeca"

#: src/components/Filters.vue:132
msgid "Language"
msgstr "Jezik"

#: src/components/VideoResult.vue:49 src/components/VideoResult.vue:50
#: src/components/VideoResult.vue:53
msgid "Language:"
msgstr "Jezik:"

#: src/components/Filters.vue:251
msgid "Last 30 days"
msgstr "Zadnjih 30 dana"

#: src/components/Filters.vue:255
msgid "Last 365 days"
msgstr "Zadnjih 365 dana"

#: src/components/Filters.vue:247
msgid "Last 7 days"
msgstr "Zadnjih 7 dana"

#: src/components/Footer.vue:11 src/components/Footer.vue:12
msgid "Learn more about PeerTube"
msgstr "Saznaj više o PeerTubeu"

#: src/components/SortButton.vue:10 src/components/SortButton.vue:11
msgid "Least recent"
msgstr "Najstariji"

#: src/components/Footer.vue:15 src/components/Footer.vue:16
msgid "Legal notices"
msgstr "Pravna pitanja"

#: src/components/Filters.vue:113
msgid "Licence"
msgstr "Licenca"

#: src/components/VideoResult.vue:6 src/components/VideoResult.vue:7
msgid "LIVE"
msgstr "UŽIVO"

#: src/components/Filters.vue:55
msgid "Live videos"
msgstr "Videa uživo"

#: src/components/Filters.vue:276
msgid "Long (> 10 min)"
msgstr "Dugi (> 10 min)"

#: src/components/Filters.vue:272
msgid "Medium (4-10 min)"
msgstr "Srednji (4 – 10 min)"

#: src/components/SortButton.vue:8 src/components/SortButton.vue:9
msgid "Most recent"
msgstr "Najnoviji"

#: src/components/Filters.vue:283
msgid "Music"
msgstr "Glazba"

#: src/components/SearchInput.vue:2 src/components/SearchInput.vue:3
msgid "My search"
msgstr "Moja pretraga"

#: src/components/Filters.vue:328
msgid "Nederlands"
msgstr "Nederlands"

#: src/components/Filters.vue:293
msgid "News & Politics"
msgstr "Vijesti i politika"

#: src/components/Pagination.vue:19 src/components/Pagination.vue:20
msgid "Next page"
msgstr "Sljedeća stranica"

#: src/components/Filters.vue:19
msgid "No"
msgstr "Ne"

#: src/views/Search.vue:36 src/views/Search.vue:37 src/views/Search.vue:40
msgid "No results found."
msgstr "Nije pronađen nijedan rezultat."

#: src/components/Filters.vue:330
msgid "Occitan"
msgstr "Occitan"

#: src/components/PlaylistResult.vue:54 src/components/PlaylistResult.vue:55
#: src/components/PlaylistResult.vue:58 src/components/VideoResult.vue:43
#: src/components/VideoResult.vue:44 src/components/VideoResult.vue:47
msgid "On platform"
msgstr "Na platformi"

#: src/components/Filters.vue:163
msgid "One of these tags"
msgstr "Jedna od ovih oznaka"

#: src/components/Filters.vue:37
msgid "Only channels"
msgstr "Samo kanale"

#: src/components/Filters.vue:42
msgid "Only playlists"
msgstr "Samo playliste"

#: src/components/Filters.vue:32
msgid "Only videos"
msgstr "Samo videa"

#: src/components/Filters.vue:175
msgid "PeerTube instance"
msgstr "PeerTube instanca"

#: src/components/Filters.vue:290
msgid "People"
msgstr "Ljudi"

#: src/components/Filters.vue:335
msgid "Polski"
msgstr "Polski"

#: src/components/Filters.vue:333
msgid "Português (Portugal)"
msgstr "Português (Portugal)"

#: src/components/Pagination.vue:3 src/components/Pagination.vue:4
msgid "Previous page"
msgstr "Prethodna stranica"

#: src/components/Filters.vue:312
msgid "Public Domain Dedication"
msgstr "Javno vlasništvo"

#: src/components/VideoResult.vue:28 src/components/VideoResult.vue:29
#: src/components/VideoResult.vue:32
msgid "Published by"
msgstr "Autor objavljivanja"

#: src/components/Filters.vue:65 src/components/Filters.vue:66
msgid "Published date"
msgstr "Datum objavljivanja"

#: src/components/Filters.vue:114 src/components/Filters.vue:115
#: src/components/Filters.vue:133 src/components/Filters.vue:134
#: src/components/Filters.vue:152 src/components/Filters.vue:153
#: src/components/Filters.vue:161 src/components/Filters.vue:162
#: src/components/Filters.vue:165 src/components/Filters.vue:26
#: src/components/Filters.vue:27 src/components/Filters.vue:49
#: src/components/Filters.vue:50 src/components/Filters.vue:68
#: src/components/Filters.vue:69 src/components/Filters.vue:7
#: src/components/Filters.vue:8 src/components/Filters.vue:82
#: src/components/Filters.vue:83 src/components/Filters.vue:95
#: src/components/Filters.vue:96
msgid "Reset"
msgstr "Resetiraj"

#: src/components/Filters.vue:297
msgid "Science & Technology"
msgstr "Znanost i tehnologija"

#: src/views/Home.vue:15 src/views/Home.vue:16
msgid ""
"Search for your favorite videos, channels and playlists on <a "
"class=\"peertube-link\" href=\"%{indexedInstancesUrl}\" "
"target=\"_blank\">%{instancesCount} PeerTube websites</a> indexed by "
"%{indexName}!"
msgstr ""
"Traži svoja omiljena videa, kanale i playliste na <a class=\"peertube-link\" "
"href=\"%{indexedInstancesUrl}\" target=\"_blank\">ma%{instancesCount} "
"PeerTube web-stranicama</a> indeksirani od %{indexName}!"

#: src/components/Footer.vue:13 src/components/Footer.vue:14
msgid "Search Index source code"
msgstr "Pretraži izvorni kod indeksa"

#: src/components/SearchInput.vue:13 src/components/SearchInput.vue:14
#: src/components/SearchInput.vue:25 src/components/SearchInput.vue:35
msgid "Search!"
msgstr "Traži!"

#: src/components/Filters.vue:268
msgid "Short (< 4 min)"
msgstr "Kratki (< 4 min)"

#: src/components/SortButton.vue:3
msgid "Sort by:"
msgstr "Razvrstaj po:"

#: src/components/Filters.vue:287
msgid "Sports"
msgstr "Sportovi"

#: src/components/Filters.vue:336
msgid "suomi"
msgstr "suomi"

#: src/components/Filters.vue:334
msgid "svenska"
msgstr "svenska"

#: src/components/Filters.vue:243
msgid "Today"
msgstr "Danas"

#: src/views/Search.vue:10 src/views/Search.vue:9
msgid "Toggle warning information"
msgstr "Uključi/isključi informacije upozorenja"

#: src/components/Filters.vue:288
msgid "Travels"
msgstr "Putovanja"

#: src/components/Filters.vue:285
msgid "Vehicles"
msgstr "Vozila"

#: src/components/Filters.vue:60
msgid "VOD videos"
msgstr "„Video na zahtjev” videa"

#: src/components/PlaylistResult.vue:114
msgid "Watch the playlist on %{host}"
msgstr "Gledaj playlistu na %{host}"

#: src/components/VideoResult.vue:123
msgid "Watch the video on %{host}"
msgstr "Gledaj video na %{host}"

#: src/components/Filters.vue:14
msgid "Yes"
msgstr "Da"

#: src/components/Filters.vue:325
msgid "ελληνικά"
msgstr "ελληνικά"

#: src/components/Filters.vue:337
msgid "русский"
msgstr "русский"

#: src/components/Filters.vue:320
msgid "日本語"
msgstr "日本語"

#: src/components/Filters.vue:332
msgid "简体中文（中国）"
msgstr "简体中文（中国）"

#~ msgid "%{ n } follower"
#~ msgid_plural "%{ n } followers"
#~ msgstr[0] "%{ n } pratitelj"
#~ msgstr[1] "%{ n } pratitelja"
#~ msgstr[2] "%{ n } pratitelja"

#~ msgid ">> Check all guides on joinpeertube.org <<"
#~ msgstr ">> Provjeri sve priručnike na joinpeertube.org <<"

#~ msgid "Create an account to take back control of your videos"
#~ msgstr "Stvori račun za preuzimanje kontrole nad tvojim videima"

#~ msgid "Go!"
#~ msgstr "Kreni!"

#~ msgid "In"
#~ msgstr "Na"

#~ msgid "Install PeerTube"
#~ msgstr "Instaliraj PeerTube"

#~ msgid "On"
#~ msgstr "Datum"

#~ msgid "Open an account on a PeerTube website"
#~ msgstr "Otvori račun na jednoj PeerTube web-stranici"

#~ msgid "Open your own videos website with PeerTube!"
#~ msgstr "Otvori vlastitu web-stranicu za videa s PeerTubeom!"

#~ msgid "Tags"
#~ msgstr "Oznake"

#~ msgid "Why should I have my own PeerTube website?"
#~ msgstr "Zašto imati vlastitu PeerTube web-stranicu?"
