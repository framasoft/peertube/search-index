export interface SearchUrl {
  search?: string
  nsfw?: string
  host?: string
  publishedDateRange?: string
  durationRange?: string
  categoryOneOf?: string[]
  licenceOneOf?: string[]
  languageOneOf?: string[]

  tagsAllOf?: string[]
  tagsOneOf?: string[]

  resultType?: 'videos' | 'channels' | 'playlists'

  isLive?: string

  sort?: string
  page?: string
}
