module.exports = {
  output: {
    path: './src/locale',
    jsonPath: '../translations',
    splitJson: true,
    linguas: false,
    locales: [
      'en_US',
      'fr_FR',
      'ca',
      'de',
      'es',
      'gl',
      'ru',
      'oc',
      'ja',
      'sv',
      'hr',
      'nl',
      'fa',
      'sr_Cyrl',
      'pl',
      'sq',
      'gd',
      'el',
      'bn',
      'it',
      'pt_BR',
      'uk',
      'cs',
      'zh_Hant'
    ]
  }
}
