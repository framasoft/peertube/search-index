import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { visualizer } from 'rollup-plugin-visualizer'
import { resolve } from 'path'

export default defineConfig({
  plugins: [
    vue(),
    visualizer({ open: true })
  ],
  server: {
    port: 8080
  },
  resolve:{
    alias: {
      '@': resolve(__dirname, 'src')
    }
  }
})
