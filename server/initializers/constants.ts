import config from 'config'
import { isTestInstance } from '../helpers/core-utils'

const API_VERSION = 'v1'

const CONFIG = {
  LISTEN: {
    PORT: config.get<number>('listen.port')
  },
  WEBSERVER: {
    SCHEME: config.get<boolean>('webserver.https') === true ? 'https' : 'http',
    HOSTNAME: config.get<string>('webserver.hostname'),
    PORT: config.get<number>('webserver.port')
  },
  MEILISEARCH: {
    HOST: config.get<string>('meilisearch.host'),
    API_KEY: config.get<string>('meilisearch.api_key'),
    INDEXES: {
      VIDEOS: config.get<string>('meilisearch.indexes.videos'),
      CHANNELS: config.get<string>('meilisearch.indexes.channels'),
      PLAYLISTS: config.get<string>('meilisearch.indexes.playlists')
    },
    FORCE_SETTINGS_UPDATE_AT_STARTUP: config.get<boolean>('meilisearch.force_settings_update_at_startup')
  },
  LOG: {
    LEVEL: config.get<string>('log.level')
  },
  SEARCH_INSTANCE: {
    NAME: config.get<string>('search-instance.name'),
    NAME_IMAGE: config.get<string>('search-instance.name_image'),
    SEARCH_IMAGE: config.get<string>('search-instance.search_image'),
    DESCRIPTION: config.get<string>('search-instance.description'),
    LEGAL_NOTICES_URL: config.get<string>('search-instance.legal_notices_url'),
    THEME: config.get<string>('search-instance.theme')
  },
  INSTANCES_INDEX: {
    URL: config.get<string>('instances-index.url'),
    PUBLIC_URL: config.get<string>('instances-index.public_url'),
    WHITELIST: {
      ENABLED: config.get<boolean>('instances-index.whitelist.enabled'),
      HOSTS: config.get<string[]>('instances-index.whitelist.hosts')
    }
  },
  API: {
    BLACKLIST: {
      ENABLED: config.get<boolean>('api.blacklist.enabled'),
      HOSTS: config.get<string[]>('api.blacklist.hosts')
    }
  },
  INDEXER: {
    HOST_CONCURRENCY: config.get<number>('indexer.host_concurrency'),
    BULK_INDEXATION_INTERVAL_MS: config.get<number>('indexer.bulk_indexation_interval_ms'),
    VIDEOS: {
      MAX_DESCRIPTION_CHARACTERS: config.get<number>('indexer.videos.max_description_characters')
    }
  }
}

const SORTABLE_COLUMNS = {
  VIDEOS_SEARCH: [ '_rankingScore', 'match', 'name', 'duration', 'createdAt', 'publishedAt', 'originallyPublishedAt', 'views', 'likes' ],
  CHANNELS_SEARCH: [ '_rankingScore', 'match', 'displayName', 'createdAt' ],
  PLAYLISTS_SEARCH: [ '_rankingScore', 'match', 'displayName', 'createdAt' ]
}

const PAGINATION_START = {
  MAX: 9000
}

const PAGINATION_COUNT = {
  DEFAULT: 20,
  MAX: 500
}

const SCHEDULER_INTERVALS_MS = {
  indexation: 60000 * 60 * 24 // 24 hours
}

const INDEXER_COUNT = 20
const INDEXER_LIMIT = 500000

const INDEXER_QUEUE_CONCURRENCY = 3

const REQUESTS = {
  MAX_RETRIES: 10,
  WAIT: 10000 // 10 seconds
}

function getWebserverUrl () {
  if (CONFIG.WEBSERVER.PORT === 80 || CONFIG.WEBSERVER.PORT === 443) {
    return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME
  }

  return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME + ':' + CONFIG.WEBSERVER.PORT
}

if (isTestInstance()) {
  SCHEDULER_INTERVALS_MS.indexation = 1000 * 60 * 5 // 5 minutes
}

export {
  getWebserverUrl,

  CONFIG,
  API_VERSION,
  PAGINATION_COUNT,
  PAGINATION_START,
  SORTABLE_COLUMNS,
  INDEXER_QUEUE_CONCURRENCY,
  SCHEDULER_INTERVALS_MS,
  INDEXER_COUNT,
  INDEXER_LIMIT,
  REQUESTS
}
