import { ActorImage } from '@peertube/peertube-types'

export type DBActorImage = Omit<ActorImage, 'createdAt' | 'updatedAt'> & {
  url: string

  // TODO: Remove on PeerTube 7.1 typings upgrade
  fileUrl: string
  createdAt: number
  updatedAt: number
}
