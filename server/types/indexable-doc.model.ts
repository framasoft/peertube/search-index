export interface IndexableDoc {
  host: string
  url: string
}
