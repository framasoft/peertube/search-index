import { VideoPlaylist } from '@peertube/peertube-types'
import { IndexableDoc } from './indexable-doc.model'
import { DBAccountSummary } from './account.model'
import { DBChannelSummary } from './channel.model'

export interface IndexablePlaylist extends VideoPlaylist, IndexableDoc {
}

export interface DBPlaylist extends Omit<VideoPlaylist, 'isLocal' | 'createdAt' | 'updatedAt' | 'ownerAccount' | 'videoChannel'> {
  indexedAt: number
  createdAt: number
  updatedAt: number

  host: string

  // Added by the query
  _rankingScore?: number

  ownerAccount: DBAccountSummary
  videoChannel: DBChannelSummary
}

// Results from the search API
export interface APIPlaylist extends VideoPlaylist {
  score: number
}
