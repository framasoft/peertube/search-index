import { Video, VideoDetails } from '@peertube/peertube-types'
import { IndexableDoc } from './indexable-doc.model'
import { DBChannel, DBChannelSummary } from './channel.model'
import { DBAccount, DBAccountSummary } from './account.model'

export interface IndexableVideo extends Video, IndexableDoc {
}

export interface IndexableVideoDetails extends VideoDetails, IndexableDoc {
  // TODO: remove when upgrading types to v6.2
  commentsPolicy: {
    id: number
    label: string
  }
}

type IgnoredVideoFields = 'isLocal' | 'createdAt' | 'updatedAt' | 'publishedAt' | 'originallyPublishedAt' | 'channel' | 'account'

interface DBVideoShared {
  indexedAt: number

  createdAt: number
  updatedAt: number
  publishedAt: number
  originallyPublishedAt: number

  host: string
  url: string

  searchableDescription: string
}

export interface DBVideo extends Omit<Video, IgnoredVideoFields>, DBVideoShared {
  account: DBAccountSummary
  channel: DBChannelSummary
}

export interface DBVideoDetails extends Omit<VideoDetails, IgnoredVideoFields>, DBVideoShared {
  account: DBAccount
  channel: DBChannel

  _rankingScore?: number

  // TODO: remove when upgrading types to v6.2
  commentsPolicy: {
    id: number
    label: string
  }
}

// Results from the search API
export interface APIVideo extends Video {
  tags: VideoDetails['tags']

  score: number
}
