import { VideoChannelsSearchQuery as PeerTubeChannelsSearchQuery } from '@peertube/peertube-types'
import { CommonSearch } from './common-search.model'

export type ChannelsSearchQuery = PeerTubeChannelsSearchQuery & CommonSearch
