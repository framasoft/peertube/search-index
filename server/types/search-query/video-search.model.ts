import { VideosSearchQuery as PeerTubeVideosSearchQuery } from '@peertube/peertube-types'
import { CommonSearch } from './common-search.model'

export type VideosSearchQuery = Omit<PeerTubeVideosSearchQuery, 'skipCount' | 'filter'> & CommonSearch & { boostLanguages: string[] }
