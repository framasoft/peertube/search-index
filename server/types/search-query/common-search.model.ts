export type CommonSearch = {
  blockedAccounts?: string[]
  blockedHosts?: string[]
  fromHost?: string
  hosts?: string[]
  filterTag?: string
  filterTagAdditionalHosts?: string[]
}
