import { Account, AccountSummary } from '@peertube/peertube-types'
import { DBActorImage } from './actor.model'

type IgnoredAccountFields = 'createdAt' | 'updatedAt' | 'avatar' | 'avatars'

interface DBAccountShared {
  handle: string

  avatar: DBActorImage
  avatars: DBActorImage[]
}

export interface DBAccount extends Omit<Account, IgnoredAccountFields> {
  handle: string

  avatar: DBActorImage
  avatars: DBActorImage[]

  createdAt: number
  updatedAt: number
}

export interface DBAccountSummary extends DBAccountShared, Omit<AccountSummary, 'avatar' | 'avatars'> {

}
