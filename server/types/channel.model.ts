import { VideoChannel, VideoChannelSummary } from '@peertube/peertube-types'
import { DBActorImage } from './actor.model'
import { IndexableDoc } from './indexable-doc.model'
import { DBAccount } from './account.model'

export interface IndexableChannel extends VideoChannel, IndexableDoc {
}

interface DBChannelShared {
  handle: string

  avatar: DBActorImage
  avatars: DBActorImage[]
}

type IgnoredChannelFields = 'ownerAccount' | 'isLocal' | 'createdAt' | 'updatedAt' | 'avatar' | 'avatars' | 'banner' | 'banners'

export interface DBChannel extends DBChannelShared, Omit<VideoChannel, IgnoredChannelFields> {
  indexedAt: number

  primaryKey: string

  ownerAccount?: DBAccount

  banner: DBActorImage
  banners: DBActorImage[]

  createdAt: number
  updatedAt: number

  _rankingScore?: number
}

export interface DBChannelSummary extends DBChannelShared, Omit<VideoChannelSummary, 'avatar' | 'avatars'> {

}

// Results from the search API
export interface APIVideoChannel extends VideoChannel {
  videosCount: number

  score: number
}
