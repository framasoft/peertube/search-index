import { CONFIG } from '../../initializers/constants'
import { listIndexInstancesHost, getMajorInstanceVersion } from '../requests/instances-index'
import { logger } from '../../helpers/logger'
import Bluebird from 'bluebird'

export async function buildInstanceHosts () {
  let indexHosts = await listIndexInstancesHost()

  if (CONFIG.INSTANCES_INDEX.WHITELIST.ENABLED) {
    const whitelistHosts = Array.isArray(CONFIG.INSTANCES_INDEX.WHITELIST.HOSTS)
      ? CONFIG.INSTANCES_INDEX.WHITELIST.HOSTS
      : []

    indexHosts = indexHosts.filter(h => whitelistHosts.includes(h))
  }

  indexHosts = await Bluebird.filter(indexHosts, async indexHost => {
    const instanceVersion = await getMajorInstanceVersion(indexHost)

    if (instanceVersion < 4) {
      logger.info(`Do not index ${indexHost} because the major version is too low (v${instanceVersion} < v4)`)

      return false
    }

    return true
  }, { concurrency: 10 })

  return indexHosts
}
