import { SearchResponse } from 'meilisearch'

export function extractSearchQueryResult (result: SearchResponse<any, any>) {
  return {
    total: result.estimatedTotalHits,
    data: result.hits
  }
}

export function buildSort (value: string) {
  if (value === '-match') return [ '_rankingScore:desc' ]
  if (value === 'match') return [ '_rankingScore:asc' ]

  if (value.substring(0, 1) === '-') {
    return [ `${value.substring(1)}:desc`, '_rankingScore:desc' ]
  }

  return [ `${value}:asc`, '_rankingScore:desc' ]
}

export function buildInQuery (term: string, values: string[] | number[]) {
  return `${term} IN ${buildInValuesArray(values)}`
}

export function buildInValuesArray (values: string[] | number[]) {
  return '[' + values.map(v => `'${v.replace(/'/g, '\\\'')}'`).join(',') + ']'
}
