import { AccountSummary, VideoChannelSummary } from '@peertube/peertube-types'
import { formatActorImageForAPI, formatActorImageForDB, formatActorImagesForAPI, formatActorImagesForDB } from './meilisearch-avatar'
import { DBAccountSummary } from '../../../types/account.model'
import { DBChannelSummary } from '../../../types/channel.model'

export function formatActorSummaryForAPI (actor: DBAccountSummary | DBChannelSummary) {
  return {
    id: actor.id,
    name: actor.name,
    displayName: actor.displayName,
    url: actor.url,
    host: actor.host,

    avatar: formatActorImageForAPI(actor.avatar),
    avatars: formatActorImagesForAPI(actor.avatars, actor.avatar)
  }
}

export function formatActorForDB (actor: AccountSummary | VideoChannelSummary) {
  return {
    id: actor.id,
    name: actor.name,
    displayName: actor.displayName,
    url: actor.url,
    host: actor.host,

    handle: `${actor.name}@${actor.host}`,

    avatar: formatActorImageForDB(actor.avatar, actor.host),
    avatars: formatActorImagesForDB(actor.avatars, actor.host)
  }
}
