import validator from 'validator'
import { buildInQuery } from '../meilisearch-queries'

export function addUUIDFilters (filters: string[], uuids: string[]) {
  if (!filters || filters.length === 0) return

  const result = {
    shortUUIDs: [] as string[],
    uuids: [] as string[]
  }

  for (const uuid of uuids) {
    if (validator.isUUID(uuid)) result.uuids.push(uuid)
    else result.shortUUIDs.push(uuid)
  }

  const parts: string[] = []
  if (result.uuids.length !== 0) parts.push(buildInQuery('uuid', result.uuids))
  if (result.shortUUIDs.length !== 0) parts.push(buildInQuery('shortUUID', result.shortUUIDs))

  filters.push(parts.join(' OR '))
}
