import { ActorImage } from '@peertube/peertube-types'
import { buildUrl } from '../../../helpers/utils'
import { DBActorImage } from '../../../types/actor.model'

export function formatActorImageForAPI (image?: DBActorImage) {
  if (!image) return null

  return {
    url: image.url,
    path: image.path,
    fileUrl: image.fileUrl,
    width: image.width,
    createdAt: new Date(image.createdAt),
    updatedAt: new Date(image.updatedAt)
  }
}

export function formatActorImagesForAPI (images?: DBActorImage[], image?: DBActorImage) {
  // Does not exist in PeerTube < 4.2
  if (!images) {
    if (!image) return []

    return [ formatActorImageForAPI(image) ]
  }

  return images.map(a => formatActorImageForAPI(a))
}

// ---------------------------------------------------------------------------

export function formatActorImageForDB (image: ActorImage & { fileUrl?: string }, host: string) {
  if (!image) return null

  return {
    url: buildUrl(host, image.path),
    path: image.path,
    fileUrl: image.fileUrl,
    width: image.width,
    createdAt: new Date(image.createdAt).getTime(),
    updatedAt: new Date(image.updatedAt).getTime()
  }
}

export function formatActorImagesForDB (images: (ActorImage & { fileUrl?: string })[], host: string) {
  if (!images) return null

  return images.map(image => formatActorImageForDB(image, host))
}
