import { logger } from '../../helpers/logger'
import { client } from '../../helpers/meilisearch'
import { buildUrl } from '../../helpers/utils'
import { CONFIG } from '../../initializers/constants'
import { DBPlaylist, APIPlaylist, IndexablePlaylist } from '../../types/playlist.model'
import { PlaylistsSearchQuery } from '../../types/search-query/playlist-search.model'
import { buildInValuesArray, buildSort, extractSearchQueryResult } from './meilisearch-queries'
import { addUUIDFilters } from './shared'
import { formatActorForDB, formatActorSummaryForAPI } from './shared/meilisearch-actor'

export async function queryPlaylists (search: PlaylistsSearchQuery) {
  const filter: string[] = [
    'videosLength != 0'
  ]

  if (search.host) filter.push(`host = '${search.host}'`)

  if (search.blockedAccounts && search.blockedAccounts.length !== 0) {
    filter.push(`ownerAccount.handle NOT IN ${buildInValuesArray(search.blockedAccounts)}`)
  }

  if (search.blockedHosts && search.blockedHosts.length !== 0) {
    filter.push(`host NOT IN ${buildInValuesArray(search.blockedHosts)}`)
  }

  if (search.hosts && search.hosts.length !== 0) {
    filter.push(`host IN ${buildInValuesArray(search.hosts)}`)
  }

  if (search.uuids) addUUIDFilters(filter, search.uuids)

  logger.debug({ filter }, 'Will query Meilisearch for playlists.')

  const result = await client.index(CONFIG.MEILISEARCH.INDEXES.PLAYLISTS).search(search.search, {
    offset: search.start,
    limit: search.count,
    sort: buildSort(search.sort),
    showRankingScore: true,
    filter
  })

  return extractSearchQueryResult(result)
}

export function formatPlaylistForAPI (p: DBPlaylist, fromHost?: string): APIPlaylist {
  if (!p.ownerAccount) return undefined
  if (!p.videoChannel) return undefined
  if (!p.privacy) return undefined
  if (!p.type) return undefined

  return {
    id: p.id,
    uuid: p.uuid,
    shortUUID: p.shortUUID,

    score: p._rankingScore,

    isLocal: fromHost === p.host,

    url: p.url,

    displayName: p.displayName,
    description: p.description,

    privacy: {
      id: p.privacy.id,
      label: p.privacy.label
    },

    videosLength: p.videosLength,

    type: {
      id: p.type.id,
      label: p.type.label
    },

    thumbnailPath: p.thumbnailPath,
    thumbnailUrl: buildUrl(p.host, p.thumbnailPath),

    embedPath: p.embedPath,
    embedUrl: buildUrl(p.host, p.embedPath),

    createdAt: new Date(p.createdAt),
    updatedAt: new Date(p.updatedAt),

    ownerAccount: formatActorSummaryForAPI(p.ownerAccount),
    videoChannel: formatActorSummaryForAPI(p.videoChannel)
  }
}

export function formatPlaylistForDB (p: IndexablePlaylist): DBPlaylist {
  return {
    id: p.id,
    uuid: p.uuid,
    shortUUID: p.shortUUID,

    indexedAt: new Date().getTime(),
    createdAt: new Date(p.createdAt).getTime(),
    updatedAt: new Date(p.updatedAt).getTime(),

    host: p.host,
    url: p.url,

    displayName: p.displayName,
    description: p.description,

    thumbnailPath: p.thumbnailPath,
    embedPath: p.embedPath,

    type: {
      id: p.type.id,
      label: p.type.label
    },
    privacy: {
      id: p.privacy.id,
      label: p.privacy.label
    },

    videosLength: p.videosLength,

    ownerAccount: formatActorForDB(p.ownerAccount),
    videoChannel: formatActorForDB(p.videoChannel)
  }
}
