import { ResultList } from '@peertube/peertube-types'
import { CONFIG } from '../../initializers/constants'
import { CommonSearch } from '../../types/search-query/common-search.model'
import { listFilteredInstanceHostsMemoized } from '../requests/instances-index'

export class Searcher <T extends CommonSearch, R, F> {

  constructor (
    private readonly queryFn: (query: T) => Promise<ResultList<R>>,
    private readonly formatFn: (data: R, fromHost: string) => F
  ) {}

  async getResult (queryArg: T): Promise<ResultList<F>> {
    const query = { ...queryArg }

    if (!Array.isArray(query.blockedHosts)) {
      query.blockedHosts = []
    }

    if (CONFIG.API.BLACKLIST.ENABLED && Array.isArray(CONFIG.API.BLACKLIST.HOSTS)) {
      query.blockedHosts = query.blockedHosts.concat(CONFIG.API.BLACKLIST.HOSTS)
    }

    if (queryArg.filterTag) {
      if (!Array.isArray(query.hosts)) query.hosts = []

      if (Array.isArray(query.filterTagAdditionalHosts)) {
        query.hosts = query.hosts.concat(query.filterTagAdditionalHosts)
      }

      query.hosts = query.hosts.concat(await listFilteredInstanceHostsMemoized(query.filterTag))

      if (query.hosts.length === 0) {
        return {
          total: 0,
          data: []
        }
      }
    }

    const resultList = await this.queryFn(query)

    return {
      total: resultList.total,
      data: resultList.data.map(d => this.formatFn(d, query.fromHost))
    }
  }
}
