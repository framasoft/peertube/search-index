import { logger } from '../../helpers/logger'
import { CONFIG, SORTABLE_COLUMNS } from '../../initializers/constants'
import { DBChannel, IndexableChannel } from '../../types/channel.model'
import { formatChannelForDB } from '../meilisearch/meilisearch-channels'
import { getChannel } from '../requests/peertube-instance'
import { AbstractIndexer } from './shared'

export class ChannelIndexer extends AbstractIndexer <IndexableChannel, DBChannel> {
  protected readonly primaryKey = 'primaryKey'
  protected readonly filterableAttributes = [ 'url', 'host', 'videosCount', 'ownerAccount.handle', 'handle' ]
  protected readonly sortableAttributes = SORTABLE_COLUMNS.CHANNELS_SEARCH
  // Keep the order, most important first
  protected readonly searchableAttributes = [ 'name', 'displayName', 'ownerAccount.name', 'ownerAccount.displayName', 'description' ]

  protected readonly rankingRules = [
    'words',
    'typo',
    'proximity',
    'attribute',
    'sort',
    'exactness',
    'followersCount:desc'
  ]

  constructor () {
    super(CONFIG.MEILISEARCH.INDEXES.CHANNELS, formatChannelForDB)
  }

  async indexSpecificElement (host: string, name: string) {
    await this.waitForBulkIndexation()

    const channel = await getChannel(host, name)

    logger.info('Indexing specific channel %s@%s.', name, host)

    this.addElementsToBulkIndex([ channel ])
  }
}
