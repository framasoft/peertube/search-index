import { CONFIG, SORTABLE_COLUMNS } from '../../initializers/constants'
import { DBPlaylist, IndexablePlaylist } from '../../types/playlist.model'
import { formatPlaylistForDB } from '../meilisearch/meilisearch-playlists'
import { AbstractIndexer } from './shared'

export class PlaylistIndexer extends AbstractIndexer <IndexablePlaylist, DBPlaylist> {
  protected readonly primaryKey = 'uuid'
  protected readonly filterableAttributes = [ 'uuid', 'host', 'ownerAccount.handle', 'ownerAccount.host', 'videosLength' ]
  protected readonly sortableAttributes = SORTABLE_COLUMNS.PLAYLISTS_SEARCH
  // Keep the order, most important first
  protected readonly searchableAttributes = [ 'displayName', 'videoChannel.displayName', 'ownerAccount.displayName', 'description' ]

  constructor () {
    super(CONFIG.MEILISEARCH.INDEXES.PLAYLISTS, formatPlaylistForDB)
  }

  async indexSpecificElement (host: string, uuid: string): Promise<any> {
    // We don't need to index a specific element yet, since we have all playlist information in the list endpoint
    throw new Error('Not implemented')
  }
}
