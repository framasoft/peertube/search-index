import { QueueObject } from 'async'
import { logger } from '../../helpers/logger'
import { AbstractIndexer, QueueParam } from './shared'
import { CONFIG, SORTABLE_COLUMNS } from '../../initializers/constants'
import { formatVideoForDB } from '../meilisearch/meilisearch-videos'
import { getVideo } from '../requests/peertube-instance'
import { DBVideo, IndexableVideo } from '../../types/video.model'

export class VideoIndexer extends AbstractIndexer <IndexableVideo, DBVideo> {
  protected readonly indexQueue: QueueObject<QueueParam>
  protected readonly primaryKey = 'uuid'
  protected readonly filterableAttributes = [
    'uuid',
    'host',
    'account.handle',
    'account.host',
    'publishedAt',
    'originallyPublishedAt',
    'nsfw',
    'category.id',
    'licence.id',
    'language.id',
    'tags',
    'duration',
    'isLive'
  ]

  protected readonly sortableAttributes = SORTABLE_COLUMNS.VIDEOS_SEARCH

  // Keep the order, most important first
  protected readonly searchableAttributes = [
    'name',
    'tags',
    'account.displayName',
    'channel.displayName',
    'searchableDescription'
  ]

  protected readonly rankingRules = [
    'words',
    'typo',
    'proximity',
    'attribute',
    'sort',
    'exactness',
    'language:asc',
    'views:desc'
  ]

  constructor () {
    super(CONFIG.MEILISEARCH.INDEXES.VIDEOS, formatVideoForDB)
  }

  async indexSpecificElement (host: string, uuid: string) {
    await this.waitForBulkIndexation()

    const video = await getVideo(host, uuid)

    logger.info('Indexing specific video %s of %s.', uuid, host)

    this.addElementsToBulkIndex([ video ])
  }
}
