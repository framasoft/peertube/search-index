import { CONFIG } from '../../initializers/constants'
import { doRequest } from '../../helpers/requests'
import memoizee from 'memoizee'
import { logger } from '../../helpers/logger'

export async function listIndexInstancesHost (): Promise<string[]> {
  const uri = CONFIG.INSTANCES_INDEX.URL

  const qs = {
    healthy: true,
    count: 5000
  }

  const { body } = await doRequest<any>({ uri, qs, json: true })

  return body.data.map(o => o.host as string)
}

export async function getMajorInstanceVersion (host: string): Promise<number> {
  try {
    const { body } = await doRequest<any>({ uri: `https://${host}/api/v1/config`, json: true })

    const version = body.serverVersion

    const majorVersion = parseInt(version.split('.')[0])

    return isNaN(majorVersion)
      ? 0
      : majorVersion
  } catch {
    return 0
  }
}

export const listFilteredInstanceHostsMemoized = memoizee(async (filterTag: string) => {
  const uri = CONFIG.INSTANCES_INDEX.URL

  const qs = {
    healthy: true,
    count: 5000,
    filterTag
  }

  const { body } = await doRequest<any>({ uri, qs, json: true })

  const hosts = body.data.map(o => o.host as string)

  logger.info({ hosts }, 'Getting filtered instance hosts for tag ' + filterTag)

  return hosts
}, { maxAge: 60000 }) // 1 minute
