import { MeiliSearch } from 'meilisearch'
import { CONFIG } from '../initializers/constants'

const client = new MeiliSearch({
  host: CONFIG.MEILISEARCH.HOST,
  apiKey: CONFIG.MEILISEARCH.API_KEY
})

export { client }
